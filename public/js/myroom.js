var MyBtn = document.getElementById("my_room");
var PublicBtn = document.getElementById("public_room");
var LogoutBtn = document.getElementById("logout");
var AddBtn = document.getElementById("BtnAddroom");
var txtRoom = document.getElementById("RoomName");
var Addarea = document.getElementById("Addarea");
var count = 0;


function init(){
    var user_email = '';
    var user_name = ' ';
    var user_front = '';
    firebase.auth().onAuthStateChanged(function(user) {
        var menu = document.getElementById('useremail');
        // Check user login
        if (user) {
            // var flag = firebase.database().ref("users").eqaulTo(user.email);
            // if(!flag){
            //     firebase.database().ref("users").push()
            // }
            user_email = user.email;
            user_name = user.displayName;
            menu.innerHTML =  user_email;
            var getindex = user_email.indexOf('@');
            user_front = user_email.substring(0, getindex);
            // firebase.database().ref(`Chatroom/userlist`).push({
            //     user: user_front
            // })
            
            //add new room div
            AddBtn.addEventListener('click', ()=>{
                if(txtRoom.value != ""){
                    var b1=-1, b2=-1, b3=-1, b4=-1;
                    var str = txtRoom.value;
                    b1 = str.indexOf('/');
                    b2 = str.indexOf('.');
                    b3 = str.indexOf('#');
                    b4 = str.indexOf('$');
                    if(b1>=0 || b2>=0 || b3>=0 || b4>=0){
                        alert("Invalid Character in room name!")
                    }
                    else{
                        // create button
                        var newroom = document.createElement("div");
                        var str = txtRoom.value;
                        str = str.replace(/</g, "&lt;");
                        str = str.replace(/>/g, "&gt;");
                        newroom.innerHTML = str;
                        newroom.setAttribute('id', 'Btn_' + count);
                        newroom.setAttribute('class', 'Btn');
                        Addarea.appendChild(newroom);

                        // add listener on every button
                        var RoomBtn = document.querySelectorAll(".Btn");
                        RoomBtn.forEach( (chatroom)=> {
                            chatroom.addEventListener('click', ()=>{
                                firebase.database().ref(`Chatroom/${chatroom.innerText}/User_list`).equalTo(user_email).once('value').then(function(e){
                                    firebase.database().ref(`Chatroom/userlist/${user_front}/now_room`).set({
                                        data: chatroom.innerText
                                    })
                                    document.location.href = "./chosen.html";
                                }).catch(function(error){
                                    alert("You can't Enter this room")
                                })
                            })
                        });

                        // push room info into room_list
                        firebase.database().ref('Chatroom/room_list').push({
                            room_name: txtRoom.value,
                            roomid: count
                        });

                        // create room database
                        firebase.database().ref(`Chatroom/${txtRoom.value}`).push({
                            room_name: txtRoom.value
                        })

                        // push current user into the room's user list
                        firebase.database().ref(`Chatroom/${txtRoom.value}/User_list`).push({
                            email: user_email
                        })
                        count++;
                        txtRoom.value = "";
                    }
                }
            })
        }
    })


    // logout
    LogoutBtn.addEventListener('click', ()=>{
        firebase.auth().signOut().then(function(){
            alert("Logout Success");
            document.location.href = "./index.html";
        }).catch(function(){
            alert("Logout failed");
        })
    })

     // enter public room
    PublicBtn.addEventListener('click', ()=>{
        document.location.href = "./public_room.html";
    })

    // enter my room
    MyBtn.addEventListener('click', ()=>{
        document.location.href = "./myroom.html";
    })

    


    var roomsRef = firebase.database().ref('Chatroom/room_list');
    var total_room = [];
    var first_count = 0;
    var second_count = 0;

    roomsRef.once('value')
        .then(function(snapshot) {

            // reload btn before
            first_count = total_room.length;
            Addarea.innerHTML = total_room.join('');

            // create new button and put it's info in database
            roomsRef.on('child_added', function(data) {
                var str_before = `<div class = "Btn" id="Btn_${data.val().roomid}">`;
                var str_after = "</div>";
                second_count += 1;
                if (second_count > first_count) {
                    var childData = data.val();
                    var str = childData.room_name;
                    str = str.replace(/</g, "&lt;");
                    str = str.replace(/>/g, "&gt;");
                    total_room[total_room.length] = str_before + str + str_after;
                    Addarea.innerHTML = total_room.join('');
                    var RoomBtn = document.querySelectorAll(".Btn");
                    RoomBtn.forEach( (chatroom)=> {
                        chatroom.addEventListener('click', ()=>{
                            firebase.database().ref(`Chatroom/${chatroom.innerText}/User_list`).orderByChild('email').equalTo(user_email).once('value', function(snapshot){
                                if(snapshot.exists()){
                                    firebase.database().ref(`Chatroom/userlist/${user_front}/now_room`).set({
                                        data: chatroom.innerText
                                    })
                                    document.location.href = "./chosen.html";
                                }
                                else{
                                    alert("You can't Enter this room")
                                }
                            })
                        })
                    });
                }
            });
        })
        .catch(e => console.log(e.message));
}


window.onload = function(){
    init();
}