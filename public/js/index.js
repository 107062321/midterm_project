function init(){
    var email = document.getElementById("email");
    var password = document.getElementById("password");
    var username = document.getElementById("username");
    var submit = document.getElementById("submit");
    
    
    // register
    submit.addEventListener('click', ()=>{
        console.log(username.value)
        firebase.auth().createUserWithEmailAndPassword(email.value,password.value).then(function(Success){
            // firebase.auth().currentUser.updateProfile({
            //     displayName: username.value
            // })
            // console.log(firebase.auth().currentUser);
            create_alert('success', 'Welcome');
        }).catch(function(error) { // Handle Errors here.
            var errorCode = error.code;
            var errorMessage = error.message;
            console.log(errorMessage);
        });
        email.value = "";
        password.value = "";
        username.value = "";
    });
    

    var email_in = document.getElementById("email_in");
    var password_in = document.getElementById("password_in");
    var submit_in = document.getElementById("submit_in");
    var signin = document.getElementById("signin");

    // Login
    
    submit_in.addEventListener("click", ()=>{
        firebase.auth().signInWithEmailAndPassword(email_in.value, password_in.value).then(()=>{
            document.location.href = "./myroom.html";
    
        }).catch(function(error) { // Handle Errors here.
            var errorCode = error.code;
            var errorMessage = error.message;
            console.log(errorMessage);
            create_alert('error', errorMessage);
        });
        email_in.value = "";
        password_in.value = "";
    })
    
    var google = document.getElementById("google");
    var provider = new firebase.auth.GoogleAuthProvider();
    
    // Google Login
    google.addEventListener('click', ()=>{
        firebase.auth().signInWithPopup(provider).then(function(result) {
            // This gives you a Google Access Token. You can use it to access the Google API. 
            var token = result.credential.accessToken;
            // The signed-in user info.
            var user = result.user;
            // ...
            document.location.href = "./myroom.html";
            }).catch(function(error) {
            // Handle Errors here.
            var errorCode = error.code;
            var errorMessage = error.message;
            // The email of the user's account used.
            var email = error.email;
            // The firebase.auth.AuthCredential type that was used. var credential = error.credential;
            create_alert('error', errorMessage);
        });
    })
}

// Custom alert
function create_alert(type, message) {
    var alertarea = document.getElementById('alert');
    if (type == "success") {
        str_html = "<div id = 'alert1' class='alert alert-success alert-dismissible fade show' role='alert'><strong>Success! </strong>" + message + "<button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button></div>";
        alertarea.innerHTML = str_html;
    } else if (type == "error") {
        str_html = "<div id = 'alert2' class='alert alert-danger alert-dismissible fade show' role='alert'><strong>Error! </strong>" + message + "<button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button></div>";
        alertarea.innerHTML = str_html;
    }
}

window.onload = function(){
    init();
}