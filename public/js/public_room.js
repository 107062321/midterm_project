var room_now;
var count=0;
function init(){
    
}


window.onload = function(){
    // get permission on sending notification
    Notification.requestPermission(function(status){
        if (Notification.permission !== status) {
            Notification.permission = status;
        }
    });

    getroom();
}

function getroom(){

    var MyBtn = document.getElementById("my_room");
    var PublicBtn = document.getElementById("public_room");
    var LogoutBtn = document.getElementById("logout");
    var SendBtn = document.getElementById("post_btn");
    var message = document.getElementById("text");
    var chatarea = document.getElementById("chat_room");
    var storageRef = firebase.storage().ref();

    var user_email = '';
    firebase.auth().onAuthStateChanged(function(user) {
        var menu = document.getElementById('useremail');
        // Check user login
        if (user) {
        // var flag = firebase.database().ref("users").eqaulTo(user.email);
            // if(!flag){
            //     firebase.database().ref("users").push()
           // }
            user_email = user.email;
            menu.innerHTML =  user.email;
            //Send Message
            var uploadFileInput = document.getElementById("uploadFileInput");
            SendBtn.addEventListener('click', ()=>{
                var now = new Date();
                var hour = now.getHours();
                var sec = now.getMinutes();
                if(message.value !== '' || uploadFileInput.files[0]){
                    if(message.value !== ''){
                        firebase.database().ref(`Chatroom/public/msg_history`).push({
                            data: message.value,
                            email: user_email,
                            photoname: "",
                            time: hour + ' : ' + sec
                        })
                    }
                    else{
                        firebase.database().ref(`Chatroom/public/msg_history`).push({
                            data: message.value,
                            email: user_email,
                            photoname: uploadFileInput.files[0].name,
                            time: hour + ' : ' + sec
                        })
                    }
                }
                message.value = "";
                uploadFileInput.value = "";
            })

            message.addEventListener('change', ()=>{
                var now = new Date();
                var hour = now.getHours();
                var sec = now.getMinutes();
                if(message.value !== '' || uploadFileInput.files[0]){
                    if(message.value !== ''){
                        firebase.database().ref(`Chatroom/public/msg_history`).push({
                            data: message.value,
                            email: user_email,
                            photoname: "",
                            time: hour + ' : ' + sec
                        })
                    }
                    else{
                        firebase.database().ref(`Chatroom/public/msg_history`).push({
                            data: message.value,
                            email: user_email,
                            photoname: uploadFileInput.files[0].name,
                            time: hour + ' : ' + sec
                        })
                    }
                }
                message.value = "";
                uploadFileInput.value = "";    
            })

            uploadFileInput.addEventListener("change", function(){
            var file = this.files[0];
            var uploadTask = storageRef.child('images/'+file.name).put(file);
            uploadTask.on('state_changed', function(snapshot){
                var progress = (snapshot.bytesTransferred / snapshot.totalBytes) * 100;
                console.log('Upload is ' + progress + '% done');
                switch (snapshot.state) {
                case firebase.storage.TaskState.PAUSED:

                    console.log('Upload is paused');
                    break;
                case firebase.storage.TaskState.RUNNING:

                    console.log('Upload is running');
                    break;
                }
            })
            })
        }
    })


    var msgsRef;
    firebase.database().ref('Chatroom/public').once("value").then(function(snapshot){
        msgsRef = firebase.database().ref(`Chatroom/public/msg_history`);
        var total_msg = [];
        var first_count = 0;
        var second_count = 0;

        msgsRef.once('value')
            .then(function(snapshot) {
                snapshot.forEach(async function(data){
                    var childData = data.val();
                    var str_before, str_after, str_middle;
                    var dataemail = childData.email;
                    var datamsg = childData.data;
                    datemail = dataemail.replace(/</g, "&lt;");
                    dataemail = dataemail.replace(/>/g, "&gt;");
                    datamsg = datamsg.replace(/</g, "&lt;");
                    datamsg = datamsg.replace(/>/g, "&gt;");
                    if(childData.email === user_email){
                        if(childData.photoname !== ""){
                            var tmp = count;
                            var tmplength = total_msg.length;
                            str_before = '<div class="media mx-2 my-3 d-flex flex-row-reverse"><img src="./img/pidaxin.jpg" class="mr-3" alt="Snoopy" style="width:48px; height:48px;"><div class="media-body" ><div class="mx-1 my-0 d-flex flex-row-reverse"><div color="white">';
                            str_middle = `</div></div><div class='d-flex flex-row-reverse'><div class='mx-1 my-0 mastermessage'><img id="img-${tmp}" style="max-width: 180px; max-height: 144px;">`;
                            str_after = '</div><div class="align-self-end" style="font-size: 8px; color: rgba(0, 0, 0, 0.4)">';
                            second_count += 1;
                            if (second_count > first_count) {
                                total_msg[total_msg.length] = str_before + dataemail + str_middle  + str_after + childData.time + "</div></div></div></div>";
                                chatarea.innerHTML = total_msg.join('');
                            }
                            var image = storageRef.child('images/'+childData.photoname);
                            var urlPromise = image.getDownloadURL();
                            console.log('urlPromise', urlPromise);
                            urlPromise.then(url => {
                                console.log('tmp-after', tmp)
                                var img = document.getElementById(`img-${tmp}`);
                                img.src = url;
                                str_before = '<div class="media mx-2 my-3 d-flex flex-row-reverse"><img src="./img/pidaxin.jpg" class="mr-3" alt="Snoopy" style="width:48px; height:48px;"><div class="media-body" ><div class="mx-1 my-0 d-flex flex-row-reverse"><div color="white">';
                                str_middle = `</div></div><div class='d-flex flex-row-reverse'><div class='mx-1 my-0 mastermessage'><img id="img-${tmp}" src = ${url} style="max-width: 180px; max-height: 144px;">`;
                                str_after = '</div><div class="align-self-end" style="font-size: 8px; color: rgba(0, 0, 0, 0.4)">';
                                total_msg[tmplength] = str_before + dataemail + str_middle  + str_after + childData.time + "</div></div></div></div>";
                                adjust(chatarea);
                            }) .catch((error) =>{
                                console.log(error.message)
                                //console.log("Fuck I am Wrong in line 142")
                            })
                        }
                        else{
                            str_before = '<div class="media mx-2 my-3 d-flex flex-row-reverse"><img src="./img/pidaxin.jpg" class="mr-3" alt="Snoopy" style="width:48px; height:48px;"><div class="media-body" ><div class="mx-1 my-0 d-flex flex-row-reverse"><div color="white">';
                            str_middle = "</div></div><div class='d-flex flex-row-reverse'><div class='mx-1 my-0 mastermessage'>";
                            str_after = '</div><div class="align-self-end" style="font-size: 8px; color: rgba(0, 0, 0, 0.4)">';
                            total_msg[total_msg.length] = str_before + dataemail + str_middle + datamsg + str_after + childData.time + "</div></div></div></div>";
                            first_count+=1;
                            chatarea.innerHTML = total_msg.join('');
                            adjust(chatarea);
                        }
                    }
                    else{
                        if(childData.photoname !== ""){
                            var tmp = count;
                            var tmplength = total_msg.length;
                            str_before = '<div class="media mx-2 my-3 d-flex flex-row"><img src="./img/snoopy.png" class="mr-3" alt="Snoopy" style="width:48px; height:48px;"><div class="media-body" ><div class="mx-1 my-0 d-flex flex-row"><div color="white">';
                            str_middle = `</div></div><div class='d-flex flex-row'><div class='mx-1 my-0 clientmessage'><img id="img-${tmp}" style="max-width: 180px; max-height: 144px;">`;
                            str_after = '</div><div class="align-self-end" style="font-size: 8px; color: rgba(0, 0, 0, 0.4)">';
                            second_count += 1;
                            if (second_count > first_count) {
                                total_msg[total_msg.length] = str_before + dataemail + str_middle  + str_after + childData.time + "</div></div></div></div>";
                                chatarea.innerHTML = total_msg.join('');
                            }
                            var image = storageRef.child('images/'+childData.photoname);
                            var urlPromise = image.getDownloadURL();
                            console.log('urlPromise', urlPromise);
                            urlPromise.then(url => {
                                console.log('tmp-after', tmp)
                                var img = document.getElementById(`img-${tmp}`);
                                img.src = url;
                                str_before = '<div class="media mx-2 my-3 d-flex flex-row"><img src="./img/snoopy.png" class="mr-3" alt="Snoopy" style="width:48px; height:48px;"><div class="media-body" ><div class="mx-1 my-0 d-flex flex-row"><div color="white">';
                                str_middle = `</div></div><div class='d-flex flex-row'><div class='mx-1 my-0 clientmessage'><img id="img-${tmp}" src = ${url} style="max-width: 180px; max-height: 144px;">`;
                                str_after = '</div><div class="align-self-end" style="font-size: 8px; color: rgba(0, 0, 0, 0.4)">';
                                total_msg[tmplength] = str_before + dataemail + str_middle  + str_after + childData.time + "</div></div></div></div>";
                                adjust(chatarea);
                            }) .catch((error) =>{
                                console.log(error.message)
                                //console.log("Fuck I am Wrong in line 142")
                            })
                        }
                        else{
                            str_before = '<div class="media mx-2 my-3 d-flex flex-row"><img src="./img/snoopy.png" class="mr-3" alt="Snoopy" style="width:48px; height:48px;"><div class="media-body" ><div class="mx-1 my-0 d-flex flex-row"><div color="white">';
                            str_middle = "</div></div><div class='d-flex flex-row'><div class='mx-1 my-0 clientmessage'>";
                            str_after = '</div><div class="align-self-end" style="font-size: 8px; color: rgba(0, 0, 0, 0.4)">';
                            total_msg[total_msg.length] = str_before + dataemail + str_middle + datamsg + str_after + childData.time + "</div></div></div></div>";
                            first_count+=1;
                            chatarea.innerHTML = total_msg.join('');
                            adjust(chatarea);
                        }
                    }
                    
                })
                // chatarea.innerHTML = total_msg.join('');

                msgsRef.on('child_added', async function(data) {
                    count += 1;
                    var childData = data.val();
                    var str_before, str_after, str_middle;
                    var dataemail = childData.email;
                    var datamsg = childData.data;
                    datemail = dataemail.replace(/</g, "&lt;");
                    dataemail = dataemail.replace(/>/g, "&gt;");
                    datamsg = datamsg.replace(/</g, "&lt;");
                    datamsg = datamsg.replace(/>/g, "&gt;");
                    if(childData.email === user_email){
                        if(childData.photoname !== ""){
                            var tmp = count;
                            var tmplength = total_msg.length;
                            str_before = '<div class="media mx-2 my-3 d-flex flex-row-reverse"><img src="./img/pidaxin.jpg" class="mr-3" alt="Snoopy" style="width:48px; height:48px;"><div class="media-body" ><div class="mx-1 my-0 d-flex flex-row-reverse"><div color="white">';
                            str_middle = `</div></div><div class='d-flex flex-row-reverse'><div class='mx-1 my-0 mastermessage'><img id="img-${tmp}" style="max-width: 180px; max-height: 144px;">`;
                            str_after = '</div><div class="align-self-end" style="font-size: 8px; color: rgba(0, 0, 0, 0.4)">';
                            second_count += 1;
                            if (second_count > first_count) {
                                total_msg[total_msg.length] = str_before + dataemail + str_middle  + str_after + childData.time + "</div></div></div></div>";
                                chatarea.innerHTML = total_msg.join('');
                            }
                            var image = storageRef.child('images/'+childData.photoname);
                            var urlPromise = image.getDownloadURL();
                            console.log('urlPromise', urlPromise);
                            urlPromise.then(url => {
                                console.log('tmp-after', tmp)
                                var img = document.getElementById(`img-${tmp}`);
                                img.src = url;
                                str_before = '<div class="media mx-2 my-3 d-flex flex-row-reverse"><img src="./img/pidaxin.jpg" class="mr-3" alt="Snoopy" style="width:48px; height:48px;"><div class="media-body" ><div class="mx-1 my-0 d-flex flex-row-reverse"><div color="white">';
                                str_middle = `</div></div><div class='d-flex flex-row-reverse'><div class='mx-1 my-0 mastermessage'><img id="img-${tmp}" src = ${url} style="max-width: 180px; max-height: 144px;">`;
                                str_after = '</div><div class="align-self-end" style="font-size: 8px; color: rgba(0, 0, 0, 0.4)">';
                                total_msg[tmplength] = str_before + dataemail + str_middle  + str_after + childData.time + "</div></div></div></div>";
                                adjust(chatarea);
                            }) .catch((error) =>{
                                console.log(error.message)
                                //console.log("Fuck I am Wrong in line 142")
                            })
                        }
                        else{
                            console.log("Hi you enter content")
                            str_before = '<div class="media mx-2 my-3 d-flex flex-row-reverse"><img src="./img/pidaxin.jpg" class="mr-3" alt="Snoopy" style="width:48px; height:48px;"><div class="media-body" ><div class="mx-1 my-0 d-flex flex-row-reverse"><div color="white">';
                            str_middle = "</div></div><div class='d-flex flex-row-reverse'><div class='mx-1 my-0 mastermessage'>";
                            str_after = '</div><div class="align-self-end" style="font-size: 8px; color: rgba(0, 0, 0, 0.4)">';
                            // total_msg[total_msg.length] = str_before + dataemail + str_middle + datamsg + str_after + childData.time + "</div></div></div></div>";
                            second_count += 1;
                            if (second_count > first_count) {
                                total_msg[total_msg.length] = str_before + dataemail + str_middle + datamsg + str_after + childData.time + "</div></div></div></div>";
                                chatarea.innerHTML = total_msg.join('');
                                adjust(chatarea);
                            }
                        }
                    }
                    else{
                        if(childData.photoname !== ""){
                            // send photo notification
                            if (Notification && Notification.permission === "granted") {
                                var Noti = new Notification("Photo", {
                                    body: childData.email  + " sends a photo"
                                });
                            }
                            var tmp = count;
                            var tmplength = total_msg.length;
                            str_before = '<div class="media mx-2 my-3 d-flex flex-row"><img src="./img/snoopy.png" class="mr-3" alt="Snoopy" style="width:48px; height:48px;"><div class="media-body" ><div class="mx-1 my-0 d-flex flex-row"><div color="white">';
                            str_middle = `</div></div><div class='d-flex flex-row'><div class='mx-1 my-0 clientmessage'><img id="img-${tmp}" style="max-width: 180px; max-height: 144px;">`;
                            str_after = '</div><div class="align-self-end" style="font-size: 8px; color: rgba(0, 0, 0, 0.4)">';
                            second_count += 1;
                            if (second_count > first_count) {
                                total_msg[total_msg.length] = str_before + dataemail + str_middle  + str_after + childData.time + "</div></div></div></div>";
                                chatarea.innerHTML = total_msg.join('');
                            }
                            var image = storageRef.child('images/'+childData.photoname);
                            var urlPromise = image.getDownloadURL();
                            console.log('urlPromise', urlPromise);
                            urlPromise.then(url => {
                                console.log('tmp-after', tmp)
                                var img = document.getElementById(`img-${tmp}`);
                                img.src = url;
                                str_before = '<div class="media mx-2 my-3 d-flex flex-row"><img src="./img/snoopy.png" class="mr-3" alt="Snoopy" style="width:48px; height:48px;"><div class="media-body" ><div class="mx-1 my-0 d-flex flex-row"><div color="white">';
                                str_middle = `</div></div><div class='d-flex flex-row'><div class='mx-1 my-0 clientmessage'><img id="img-${tmp}" src = ${url} style="max-width: 180px; max-height: 144px;">`;
                                str_after = '</div><div class="align-self-end" style="font-size: 8px; color: rgba(0, 0, 0, 0.4)">';
                                total_msg[tmplength] = str_before + dataemail + str_middle  + str_after + childData.time + "</div></div></div></div>";
                                adjust(chatarea);
                            }) .catch((error) =>{
                                console.log(error.message)
                                //console.log("Fuck I am Wrong in line 142")
                            })
                        }
                        else{
                            // send message notofication
                            if (Notification && Notification.permission === "granted") {
                                var Noti = new Notification("Message", {
                                    body: childData.email + " says: " + childData.data 
                                });
                            }
                            str_before = '<div class="media mx-2 my-3 d-flex flex-row"><img src="./img/snoopy.png" class="mr-3" alt="Snoopy" style="width:48px; height:48px;"><div class="media-body" ><div class="mx-1 my-0 d-flex flex-row"><div color="white">';
                            str_middle = "</div></div><div class='d-flex flex-row'><div class='mx-1 my-0 clientmessage'>";
                            str_after = '</div><div class="align-self-end" style="font-size: 8px; color: rgba(0, 0, 0, 0.4)">';
                            second_count += 1;
                            if (second_count > first_count) {
                                total_msg[total_msg.length] = str_before + dataemail + str_middle + datamsg + str_after + childData.time + "</div></div></div></div>";
                                chatarea.innerHTML = total_msg.join('');
                                adjust(chatarea);
                            }
                        }
                    }
                });
            })
            .catch(e => console.log(e.message));
    })

     // logout
     LogoutBtn.addEventListener('click', ()=>{
        firebase.auth().signOut().then(function(){
            alert("Logout Success");
            document.location.href = "./index.html";
        }).catch(function(){
            alert("Logout failed");
        })
    })

    // enter public room
    PublicBtn.addEventListener('click', ()=>{
        document.location.href = "./public_room.html";
    })

    // enter my room
    MyBtn.addEventListener('click', ()=>{
        document.location.href = "./myroom.html";
    })


   
}
function adjust(chatarea){
   chatarea.scrollTop = chatarea.scrollHeight;
}