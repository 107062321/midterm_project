# Software Studio 2020 Spring Midterm Project

## Topic
* Project Name : Midterm-Project
* Key functions (add/delete)
    1. 登入頁面：
        + 可以利用電子郵件註冊一個帳戶
        + 也可以利用第三方(google)登入
        + 有了帳號後可以進行登入的動作
    2. 大廳畫面
        + 可以輸入房間名稱後並創建一個房間
        + 在房間列表中可以看到所有房間
        + 點擊房間後，若使用者有在此房間的使用者名單內，即可進入
        + 上方navbar中，點選logout即可登出
    3. 私人聊天室
        + 在房間內可在下方的輸入框中，輸入文字後送出
        + 可以即時更新聊天訊息
        + 可在退出後，或者重新刷新頁面後，重新Load出歷史訊息
        + 可在上方的navbar中的輸入欄中，輸入其他使用者的email，將其加入
        + 上方navbar中，點選logout即可登出
    
* Other functions (add/delete)
    1. 公共聊天室：
        + 任何只要用帳號的user都可以在這個聊天室內任意發言
        + 其餘的功能都與私人聊天室一樣
    2. 聊天室訊息：
        + 聊天室內的文字訊息只要被發送，聊天頁面會自動跳到最底部
        + 文字訊息會根據你輸入的長度不同，而產生不同大小的對話框
        + 可以看到傳送訊息的時間
    3. 圖片訊息
        + 可以從裝置中選擇圖片訊息，並且傳送至聊天室

## Basic Components
|Component|Score|Y/N|
|:-:|:-:|:-:|
|Membership Mechanism|15%|Y|
|Firebase Page|5%|Y|
|Database|15%|Y|
|RWD|15%|Y|
|Topic Key Function|20%|Y|

## Advanced Components
|Component|Score|Y/N|
|:-:|:-:|:-:|
|Third-Party Sign In|2.5%|Y|
|Chrome Notification|5%|Y|
|Use CSS Animation|2.5%|Y|
|Security Report|5%|Y|

## Website Detail Description

### 登入頁面
![](https://i.imgur.com/kn4zSWl.jpg)
+ 在sign up的地方使用email帳號來進行註冊
+ 在sign in的地方輸入email和password來進行登入
+ 透過第三方(google)來進行登入

### 大廳頁面
![](https://i.imgur.com/MMiarzq.jpg)
+ 在上方的navbar中，my room指的就是這個大廳頁面，而public room就是可以跳到一個大家都可以自由進入的public room。 而logout就能登出。
+ 中間的輸入框中，可以輸入你要命名的房間名稱，然後點選Add後就可以加入了

+ 而下方欄位中就會出現現在有的房間，若你有在此房間的userlist內，你就可以進入此私人聊天室了，若還不在userlist中，那就會跳出alert還不能進入房間。

### 私人聊天室
![](https://i.imgur.com/25RYdr2.jpg)
+ 在上方的navbar中，有一個輸入欄位是可以讓在這個房間中的使用者，輸入其他使用者的email，點選後方的Add user，即可將其他使用者也加入此聊天室。

+ 在上方的navbar也可以看到自己現在是處在哪一個聊天室。
+ 下方的一個大頁面就是可以看到聊天的視窗。
+ 訊息的內容包括傳送者的email，傳送的時間，傳送的內容。
+ 在視窗的下方，有一個選擇檔案，可以讓使用者選擇裝置中的照片，等一段時間上傳好後，就可以傳送出去。
+ 在選擇檔案的下方，有一個輸入訊息的訊息欄，輸入完後，可以按下方的Send或者直接點擊Enter也可以送出。（圖片只能用Send）
+ 若你有網頁停留在此聊天室中，有人即時的傳了訊息給你，那就會跳出一個notification，來提醒使用者有訊息傳送給你了。![](https://i.imgur.com/DQsmt3V.png)

### 公共聊天室
![](https://i.imgur.com/fGZhZvV.jpg)
+ 公共聊天室的功能與私人聊天室一樣，只是無法加入其他使用者(因為有帳號的人都能進來)





# 作品網址：
https://midterm-project-s107062321.web.app/

# Components Description : 
1. RWD : 根據視窗左右大小的改變，亦或者是從手機上來看，網頁上的物件不會重疊。
2. Database : 在database上主要紀錄的
    + 房間的列表
    + 每個不同的房間的資訊，例如：聊天的訊息內容、傳送訊息的時間、傳送訊息的用戶的email、此房間的User_list
    + 現在此用戶所在的房間

3. CSS animation ： 在登入的介面中，背景的地方會有幾隻鳥飛過去，讓使用者在辦帳號或輸入帳密的時候，不會感到孤單！

# Other Functions Description : 
1. Storage : 要儲存照片到Firebase上，要使用到Firebase的Storage，將圖片從本地端Load到Storage後，之後有需要的時候就可以取用出來

2. Logout : 可以登出防止別人亂用別人亂使用自己的帳戶
3. Chrome notification : 當你網頁所在的聊天室內，有人傳訊息給你，會有google通知你有訊息傳過來
4. Send photo : 可以做到Database做不到的傳送圖片的功能，可以讓使用者在裝置上選取圖片，並且傳送到聊天室
5. Add user : 可以把其他使用者加到此聊天室
6. Public room : 可以有一個，只要有帳號都可以進入的公共聊天室。

## Security Report (Optional)
1. 可以防止有心人士在命名房間的地方的輸入，和傳送訊息的輸入的地方，輸入html的code，導致網頁的版面壞掉。

2. 避免有心人士，所以登出後即使用回到上一頁的功能回到private room，那還是沒有辦法看到訊息和發送文字。回到大廳也不能創建房間，到public room只能看到訊息，也不能發送訊息。 